<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    public $primaryKey = 'id_pengaduan';
	protected $table = 'pengaduan';

	protected $fillable = ['id_guru', 'id_teknisi', 'id_ruangan', 'deskripsi'];
}
