<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RuanganController extends Controller
{
    public function index()
    {
    	$data['result'] = \App\Ruangan::all();
    	return view('ruangan/ruangan')->with($data);
    }

    public function create()
    {
    	return view('ruangan/form');
    }

    public function store(Request $request)
    {
    	$rules = [
    		'kode_ruangan'	=> 'required|max:100',
    		'nama_ruangan'	=> 'required|max:100',
    		'lokasi'	=> 'required|max:100',
    		'kapasitas'	=> 'required|max:10',
    	];
    	$this->validate($request, $rules);

    	$input = $request->all();

    	$status = \App\Ruangan::create($input);

    	if ($status) return redirect('data-ruangan')->with('success', 'Data Berhasil Ditambahkan');
    	else return redirect('data-ruangan')->with('error', 'Data Gagal Ditambahkan');
    }

    public function edit($id)
    {
    	$data['result'] = \App\Ruangan::where('id_ruangan', $id)->first();
    	return view('ruangan/form')->with($data);
    }

    public function update(Request $request, $id)
    {
    	$rules = [
    		'kode_ruangan'	=> 'required|max:100',
    		'nama_ruangan'	=> 'required|max:100',
    		'lokasi'	=> 'required|max:100',
    		'kapasitas'	=> 'required|max:10'
    	];
    	$this->validate($request, $rules);

    	$input = $request->all();
    	$result = \App\Ruangan::where('id_ruangan', $id)->first();

    	$status = $result->update($input);

    	if ($status) return redirect('data-ruangan')->with('success', 'Data Berhasil Ditambahkan');
    	else return redirect('data-ruangan')->with('error', 'Data Gagal Ditambahkan');
    }

    public function destroy(Request $request, $id)
    {
        $result = \App\Ruangan::where('id_ruangan', $id)->first();
        $status = $result->delete();

        if ($status) return redirect('data-ruangan')->with('success', 'Data Berhasil Dihapus');
        else return redirect('data-ruangan')->with('error', 'Data Gagal Dihapus');
    }
}
