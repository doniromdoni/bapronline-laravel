<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('home-pengaduan');
    }

    public function store(Request $request)
    {
        $rules = [
            'id_guru'       => 'required',
            'id_ruangan'    => 'required',
            'deskripsi'     => 'required',
        ];
        $this->validate($request, $rules);

        $input = $request->all();

        $status = \App\Pengaduan::create($input);

        if ($status) return redirect('/')->with('success', 'Formulir Pengaduan Kerusakan Berhasil Ditambahkan');
        else return redirect('/')->with('error', 'Data Gagal Ditambahkan');
    }
}
