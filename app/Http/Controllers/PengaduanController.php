<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PengaduanController extends Controller
{
    public function index()
    {
    	$data['result'] = \App\Pengaduan::all();
    	return view('kerusakan/kerusakan')->with($data);
    }

    public function edit($id)
    {
    	$data['result'] = \App\Pengaduan::where('id_pengaduan', $id)->first();
    	return view('kerusakan/form')->with($data);
    }

    public function update(Request $request, $id)
    {
    	$rules = [
    		'id_guru'		=> 'required',
    		'id_teknisi'	=> 'required',
    		'id_ruangan'	=> 'required',
    		'deskripsi'		=> 'required',
    		'status'		=> 'required',
    	];
    	$this->validate($request, $rules);

    	$input = $request->all();
    	$result = \App\Pengaduan::where('id_pengaduan', $id)->first();

    	$status = $result->update($input);

    	if ($status) return redirect('data-kerusakan')->with('success', 'Data Berhasil Ditambahkan');
    	else return redirect('data-kerusakan')->with('error', 'Data Gagal Ditambahkan');
    }

    public function destroy(Request $request, $id)
    {
        $result = \App\Pengaduan::where('id_pengaduan', $id)->first();
        $status = $result->delete();

        if ($status) return redirect('data-kerusakan')->with('success', 'Data Berhasil Dihapus');
        else return redirect('data-kerusakan')->with('error', 'Data Gagal Dihapus');
    }
}
