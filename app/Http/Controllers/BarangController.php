<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function index()
    {
    	$data['result'] = \App\Barang::all();
    	return view('barang/barang')->with($data);
    }

    public function create()
    {
    	return view('barang/form');
    }

    public function store(Request $request)
    {
    	$rules = [
    		'kode_barang'	=> 'required|max:100',
    		'nama_barang'	=> 'required|max:100',
    		'kondisi'		=> 'required|max:100',
    		'qty'			=> 'required|max:10',
    	];
    	$this->validate($request, $rules);

    	$input = $request->all();

    	$status = \App\Barang::create($input);

    	if ($status) return redirect('data-barang')->with('success', 'Data Berhasil Ditambahkan');
    	else return redirect('data-barang')->with('error', 'Data Gagal Ditambahkan');
    }

    public function edit($id)
    {
    	$data['result'] = \App\Barang::where('id_barang', $id)->first();
    	return view('barang/form')->with($data);
    }

    public function edited(Request $request, $id)
    {
    	$rules = [
    		'kode_barang'	=> 'required|max:100',
    		'nama_barang'	=> 'required|max:100',
    		'kondisi'		=> 'required|max:100',
    		'qty'			=> 'required|max:10',
    	];
    	$this->validate($request, $rules);

    	$input = $request->all();
    	$result = \App\Barang::where('id_barang', $id)->first();

    	$status = $result->update($input);

    	if ($status) return redirect('data-barang')->with('success', 'Data Berhasil Ditambahkan');
    	else return redirect('data-barang')->with('error', 'Data Gagal Ditambahkan');
    }

    public function destroy(Request $request, $id)
    {
        $result = \App\Barang::where('id_barang', $id)->first();
        $status = $result->delete();

        if ($status) return redirect('data-barang')->with('success', 'Data Berhasil Dihapus');
        else return redirect('data-barang')->with('error', 'Data Gagal Dihapus');
    }

}
