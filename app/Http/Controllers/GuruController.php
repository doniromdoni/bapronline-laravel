<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuruController extends Controller
{
    public function index()
    {
    	$data['result'] = \App\Guru::all();
    	return view('guru/guru')->with($data);
    }

    public function create()
    {
    	return view('guru/form');
    }

    public function store(Request $request)
    {
    	$rules = [
    		'id_guru'		=> 'required|max:100',
    		'id_user'		=> 'required|max:100',
    		'nama_guru'		=> 'required|max:100',
    		'pendidikan'	=> 'required|max:100',
    		'jabatan'		=> 'required|max:100',
    	];
    	$this->validate($request, $rules);

    	$input = $request->all();

    	$status = \App\Guru::create($input);

    	if ($status) return redirect('data-guru')->with('success', 'Data Berhasil Ditambahkan');
    	else return redirect('data-guru')->with('error', 'Data Gagal Ditambahkan');
    }

    public function edit($id)
    {
    	$data['result'] = \App\Guru::where('id_guru', $id)->first();
    	return view('guru/form')->with($data);
    }

    public function update(Request $request, $id)
    {
    	$rules = [
    		'id_guru'		=> 'required|max:100',
    		'id_user'		=> 'required|max:100',
    		'nama_guru'		=> 'required|max:100',
    		'pendidikan'	=> 'required|max:100',
    		'jabatan'		=> 'required|max:100',
    	];
    	$this->validate($request, $rules);

    	$input = $request->all();
    	$result = \App\Guru::where('id_guru', $id)->first();

    	$status = $result->update($input);

    	if ($status) return redirect('data-guru')->with('success', 'Data Berhasil Ditambahkan');
    	else return redirect('data-guru')->with('error', 'Data Gagal Ditambahkan');
    }

    public function destroy(Request $request, $id)
    {
        $result = \App\Guru::where('id_guru', $id)->first();
        $status = $result->delete();

        if ($status) return redirect('data-guru')->with('success', 'Data Berhasil Dihapus');
        else return redirect('data-guru')->with('error', 'Data Gagal Dihapus');
    }

}
