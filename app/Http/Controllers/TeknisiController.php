<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeknisiController extends Controller
{
    public function index()
    {
    	$data['result'] = \App\Teknisi::all();
    	return view('teknisi/teknisi')->with($data);
    }

    public function create()
    {
    	return view('teknisi/form');
    }

    public function store(Request $request)
    {
    	$rules = [
    		'id_teknisi'	=> 'required|max:100',
    		'id_user'	=> 'required|max:100',
    		'nama_teknisi'	=> 'required|max:100',
    		'divisi'	=> 'required|max:10',
    	];
    	$this->validate($request, $rules);

    	$input = $request->all();

    	$status = \App\Teknisi::create($input);

    	if ($status) return redirect('data-teknisi')->with('success', 'Data Berhasil Ditambahkan');
    	else return redirect('data-teknisi')->with('error', 'Data Gagal Ditambahkan');
    }

    public function edit($id)
    {
    	$data['result'] = \App\Teknisi::where('id_teknisi', $id)->first();
    	return view('teknisi/form')->with($data);
    }

    public function update(Request $request, $id)
    {
    	$rules = [
    		'id_teknisi'	=> 'required|max:100',
    		'id_user'	=> 'required|max:100',
    		'nama_teknisi'	=> 'required|max:100',
    		'divisi'	=> 'required|max:10',
    	];
    	$this->validate($request, $rules);

    	$input = $request->all();
    	$result = \App\Teknisi::where('id_teknisi', $id)->first();

    	$status = $result->update($input);

    	if ($status) return redirect('data-teknisi')->with('success', 'Data Berhasil Ditambahkan');
    	else return redirect('data-teknisi')->with('error', 'Data Gagal Ditambahkan');
    }

    public function destroy(Request $request, $id)
    {
        $result = \App\Teknisi::where('id_teknisi', $id)->first();
        $status = $result->delete();

        if ($status) return redirect('data-teknisi')->with('success', 'Data Berhasil Dihapus');
        else return redirect('data-teknisi')->with('error', 'Data Gagal Dihapus');
    }
}
