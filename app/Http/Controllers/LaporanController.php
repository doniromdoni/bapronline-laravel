<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index()
    {
    	$data['result'] = \App\Laporan::all();
    	return view('laporan')->with($data);
    }
}
