<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
    	$data['result'] = \App\Ruangan::all();
    	return view('dashboard/dashboard')->with($data);
    }

}
