<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    public $primaryKey = 'id_barang';
	protected $table = 'barang';

	protected $fillable = ['kode_barang','nama_barang', 'kondisi', 'qty'];
}
