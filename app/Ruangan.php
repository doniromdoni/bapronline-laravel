<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
	public $primaryKey = 'id_ruangan';
	protected $table = 'ruangan';

	protected $fillable = ['kode_ruangan', 'nama_ruangan', 'lokasi', 'kapasitas'];
}
