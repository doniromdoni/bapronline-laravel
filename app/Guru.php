<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    public $primaryKey = 'id_guru';
    public $foreignKey = 'id_user';
	protected $table = 'guru';

	protected $fillable = ['id_guru', 'id_user', 'nama_guru', 'pendidikan', 'jabatan'];
}
