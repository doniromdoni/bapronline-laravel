<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengaduan extends Model
{
    public $primaryKey = 'id_pengaduan';
	protected $table = 'pengaduan';

	protected $fillable = ['id_guru', 'id_teknisi', 'status', 'id_ruangan', 'deskripsi'];
}
