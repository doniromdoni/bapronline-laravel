<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teknisi extends Model
{
    public $primaryKey = 'id_teknisi';
    public $foreignKey = 'id_user';
	protected $table = 'teknisi';

	protected $fillable = ['id_teknisi', 'id_user', 'nama_teknisi', 'divisi', 'foto'];
}
