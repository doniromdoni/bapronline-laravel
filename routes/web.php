<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@index');
Route::get('laporan', 'LaporanController@index');
Route::post('/add', 'HomeController@store');

Route::auth();
Route::group(['middleware'=>'auth'],function(){

Route::get('dashboard', 'DashboardController@index');

Route::get('data-ruangan', 'RuanganController@index');
Route::get('ruangan/add', 'RuanganController@create');
Route::post('ruangan/add', 'RuanganController@store');
Route::get('ruangan/{id}/edit', 'RuanganController@edit');
Route::patch('ruangan/{id}/edit', 'RuanganController@update');
Route::delete('ruangan/{id}/delete', 'RuanganController@destroy');

Route::get('data-teknisi', 'TeknisiController@index');
Route::get('teknisi/add', 'TeknisiController@create');
Route::post('teknisi/add', 'TeknisiController@store');
Route::get('teknisi/{id}/edit', 'TeknisiController@edit');
Route::patch('teknisi/{id}/edit', 'TeknisiController@update');
Route::delete('teknisi/{id}/delete', 'TeknisiController@destroy');

Route::get('data-guru', 'GuruController@index');
Route::get('guru/add', 'GuruController@create');
Route::post('guru/add', 'GuruController@store');
Route::get('guru/{id}/edit', 'GuruController@edit');
Route::patch('guru/{id}/edit', 'GuruController@update');
Route::delete('guru/{id}/delete', 'GuruController@destroy');

Route::get('data-barang', 'BarangController@index');
Route::get('barang/add', 'BarangController@create');
Route::post('barang/add', 'BarangController@store');
Route::get('barang/{id}/edit', 'BarangController@edit');
Route::patch('barang/{id}/edit', 'BarangController@edited');
Route::delete('barang/{id}/delete', 'BarangController@destroy');

Route::get('data-kerusakan', 'PengaduanController@index');
Route::get('kerusakan/{id}/edit', 'PengaduanController@edit');
Route::patch('kerusakan/{id}/edit', 'PengaduanController@update');
Route::delete('kerusakan/{id}/delete', 'PengaduanController@destroy');
});