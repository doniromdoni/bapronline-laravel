  @extends('assets/header')
  @section('content')
    <section class="content-header">
      <h1>Data Teknisi</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Teknisi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @include('assets/feedback')
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <tr>
                  <td>
                    <a href="{{ url('teknisi/add') }}" class="btn btn-social btn-primary"><i class="fa fa-plus"></i> Tambah Data Teknisi</a>
                  </td>
                  <td>
                    <a onclick="window.location.reload()" class="btn btn-success"><i class="fa fa-refresh"></i> Refresh</a>
                  </td>
              </tr>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>ID Teknisi</th>
                  <th>Nama Teknisi</th>
                  <th>Divisi</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($result as $row)
                <tr>
                  <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                  <td>{{ $row->id_teknisi  }}</td>
                  <td>{{ $row->nama_teknisi }}</td>
                  <td>{{ $row->divisi }}</td>
                  <td>                        
                        <a href="{{ url("teknisi/$row->id_teknisi/edit") }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                        
                        <form action="{{ url("teknisi/$row->id_teknisi/delete") }}" method="POST" style="display:inline;">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                        </form>
                      </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  @endsection