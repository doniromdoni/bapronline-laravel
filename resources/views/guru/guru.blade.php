  @extends('assets/header')
  @section('content')
    <section class="content-header">
      <h1>Data Guru</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Data Guru</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @include('assets/feedback')
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <tr>
                  <td>
                    <a href="{{ url('guru/add') }}" class="btn btn-social btn-primary"><i class="fa fa-plus"></i> Tambah Data Guru</a>
                  </td>
                  <td>
                    <a onclick="window.location.reload()" class="btn btn-success"><i class="fa fa-refresh"></i> Refresh</a>
                  </td>
              </tr>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Lengkap</th>
                  <th>Pendidikan</th>
                  <th>Jabatan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($result as $row)
                <tr>
                  <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                  <td>{{ $row->nama_guru  }}</td>
                  <td>{{ $row->pendidikan }}</td>
                  <td>{{ $row->jabatan }}</td>
                  <td>                        
                        <a href="{{ url("guru/$row->id_guru/edit") }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                        
                        <form action="{{ url("guru/$row->id_guru/delete") }}" method="POST" style="display:inline;">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                        </form>
                      </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  @endsection