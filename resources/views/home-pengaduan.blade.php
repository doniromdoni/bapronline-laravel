<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BAPR ONLINE</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="{{ asset('assets') }}/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('assets') }}/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets') }}/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('assets') }}/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  @stack('style')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">BAPR</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>BAPR</b>ONLINE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="{{ url('laporan') }}">
              <span>Laporan <i class="fa fa-book"></i></span>
            </a>
          </li>
          <li class="dropdown user user-menu">
            <a href="{{ url('login') }}">
              <span>Login Admin <i class="fa fa-sign-in"></i></span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
    <!-- Main content -->
    <section class="content" style="background: #fff">
      @include('assets/feedback')
      <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
          <section>
            <h2 class="text-center">
              Formulir Pengaduan Kerusakan
            </h2>
          </section>
          <div class="box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="{{ url('/add') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                  <label class="control-label col-sm-2">ID Guru</label>
                  <div class="col-sm-8">
                    <input type="number" name="id_guru" class="form-control" placeholder="ID Guru"/>
                  </div>
                </div>

                <input type="hidden" name="id_teknisi" value="0" />
                <input type="hidden" name="status" value="0" />

                <div class="form-group">
                  <label class="control-label col-sm-2">Ruangan</label>
                  <div class="col-sm-8">
                    <select name="id_ruangan" class="form-control">
                      @foreach (\App\Ruangan::all() as $ruangan)
                      <option value="{{ $ruangan->id_ruangan }}" {{ @$result->id_ruangan == $ruangan->id_ruangan ? 'selected' : '' }}>{{ $ruangan->nama_ruangan }}</option>
                      @endforeach
                    </select>  
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2">Deskripsi</label>
                  <div class="col-sm-8">
                    <textarea name="deskripsi" class="form-control" placeholder="Deskripsi Kerusakan"></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"> Upload</i></button>
                  </div>
                </div>

              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset('assets') }}/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ asset('assets') }}/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="{{ asset('assets') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('assets') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets') }}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{ asset('assets') }}/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets') }}/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets') }}/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
</body>
</html>