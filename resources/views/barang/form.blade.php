  @extends('assets/header')
  @section('content')
    <section class="content-header">
      <h1>
        {{ empty($result) ? 'Tambah' : 'Edit' }} Data Barang
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="{{ url('data-barang') }}">Data Barang</a></li>
        <li class="active">{{ empty($result) ? 'Tambah' : 'Edit' }} Data Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @include('assets/feedback')
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <tr>
                  <td>
                    <a href="{{ url('data-barang') }}" class="btn bg-purple"><i class="fa fa-chevron-left"></i> Kembali</a>
                  </td>
              </tr>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="{{ empty($result) ? url('barang/add') : url('barang/$result->id_barang/edit') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                @if (!empty($result))
                  {{ method_field('patch') }}
                @endif
                <div class="form-group">
                  <label class="control-label col-sm-2">Kode Barang</label>
                  <div class="col-sm-8">
                    <input type="text" name="kode_barang" class="form-control" placeholder="Kode Barang" value="{{ @$result->kode_barang }}" />
                  </div>
                </div>

                <!--<div class="form-group">
                  <label class="control-label col-sm-2">Kode Ruangan</label>
                  <div class="col-sm-8">
                    <select name="id_ruangan" class="form-control">
                      @foreach (\App\Ruangan::all() as $ruangan)
                      <option value="{{ $ruangan->id_ruangan }}" {{ @$result->id_ruangan == $ruangan->id_ruangan ? 'selected' : '' }}>{{ $ruangan->nama_ruangan }}</option>
                      @endforeach
                    </select>  
                  </div>
                </div>-->

                <div class="form-group">
                  <label class="control-label col-sm-2">Nama Barang</label>
                  <div class="col-sm-8">
                    <input type="text" name="nama_barang" class="form-control" placeholder="Nama Barang" value="{{ @$result->nama_barang }}"/>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2">Kondisi Barang</label>
                  <div class="col-sm-8">
                    <input type="text" name="kondisi" class="form-control" placeholder="Kondisi Barang" value="{{ @$result->kondisi }}"/>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2">Qty</label>
                  <div class="col-sm-8">
                    <input type="number" name="qty" class="form-control" placeholder="Qty Barang" value="{{ @$result->qty }}"/>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"> Simpan</i></button>
                  </div>
                </div>

              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  @endsection