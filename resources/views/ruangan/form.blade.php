  @extends('assets/header')
  @section('content')
    <section class="content-header">
      <h1>
        {{ empty($result) ? 'Tambah' : 'Edit' }} Data Ruangan
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="{{ url('data-ruangan') }}">Data Ruangan</a></li>
        <li class="active">{{ empty($result) ? 'Tambah' : 'Edit' }} Data Ruangan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @include('assets/feedback')
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <tr>
                  <td>
                    <a href="{{ url('data-ruangan') }}" class="btn bg-purple"><i class="fa fa-chevron-left"></i> Kembali</a>
                  </td>
              </tr>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="{{ empty($result) ? url('ruangan/add') : url('ruangan/$result->id_ruangan/edit') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                @if (!empty($result))
                  {{ method_field('patch') }}
                @endif
                <div class="form-group">
                  <label class="control-label col-sm-2">Kode Ruangan</label>
                  <div class="col-sm-8">
                    <input type="text" name="kode_ruangan" class="form-control" placeholder="Kode Ruangan" value="{{ @$result->kode_ruangan }}" />
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2">Nama Ruangan</label>
                  <div class="col-sm-8">
                    <input type="text" name="nama_ruangan" class="form-control" placeholder="Nama Ruangan" value="{{ @$result->nama_ruangan }}" />
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2">Lokasi</label>
                  <div class="col-sm-8">
                    <input type="text" name="lokasi" class="form-control" placeholder="Lokasi Ruangan" value="{{ @$result->lokasi }}"/>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2">Kapasitas Ruangan</label>
                  <div class="col-sm-8">
                    <input type="text" name="kapasitas" class="form-control" placeholder="Kapasitas Ruangan" value="{{ @$result->kapasitas }}"/>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"> Simpan</i></button>
                  </div>
                </div>

              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  @endsection