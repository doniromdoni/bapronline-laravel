  @extends('assets/header')
  @section('content')
    <section class="content-header">
      <h1>
        {{ empty($result) ? 'Tambah' : 'Edit' }} Data Kerusakan Barang
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="{{ url('data-barang') }}">Data kerusakan Barang</a></li>
        <li class="active">{{ empty($result) ? 'Tambah' : 'Edit' }} Data Kerusakan Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @include('assets/feedback')
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <tr>
                  <td>
                    <a href="{{ url('data-kerusakan') }}" class="btn bg-purple"><i class="fa fa-chevron-left"></i> Kembali</a>
                  </td>
              </tr>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="{{ empty($result) ? url('kerusakan/add') : url('kerusakan/$result->id_pengaduan/edit') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                @if (!empty($result))
                  {{ method_field('patch') }}
                @endif
                <div class="form-group">
                  <label class="control-label col-sm-2">ID Guru</label>
                  <div class="col-sm-8">
                    <input type="number" name="id_guru" class="form-control" placeholder="ID Guru" value="{{ @$result->id_guru }}" />
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2">Teknisi</label>
                  <div class="col-sm-8">
                    <select name="id_teknisi" class="form-control">
                      @foreach (\App\Teknisi::all() as $teknisi)
                      <option value="{{ $teknisi->id_teknisi }}" {{ @$result->id_teknisi == $teknisi->id_teknisi ? 'selected' : '' }}>{{ $teknisi->nama_teknisi }}</option>
                      @endforeach
                    </select>  
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2">Ruangan</label>
                  <div class="col-sm-8">
                    <select name="id_ruangan" class="form-control">
                      @foreach (\App\Ruangan::all() as $ruangan)
                      <option value="{{ $ruangan->id_ruangan }}" {{ @$result->id_ruangan == $ruangan->id_ruangan ? 'selected' : '' }}>{{ $ruangan->nama_ruangan }}</option>
                      @endforeach
                    </select>  
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2">Deskripsi</label>
                  <div class="col-sm-8">
                    <input type="text" name="deskripsi" class="form-control" placeholder="Deskripsi Barang" value="{{ @$result->deskripsi }}"/>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2">Status</label>
                  <div class="col-sm-8">
                    <input type="text" name="status" class="form-control" placeholder="Status Barang" value="{{ @$result->status }}"/>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"> Simpan</i></button>
                  </div>
                </div>

              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  @endsection